#!/usr/bin/bash

rm -f counter.rrd

# On part du début lz 01 fevrier 2020
date_beg=$(date -d "2020-02-01 10:00:00" +"%s")

rrdtool create counter.rrd --step 1m --start $((date_beg-1)) \
    DS:ifOutOctets:COUNTER:120:0:U \
    RRA:AVERAGE:0.5:1m:10 \
    RRA:AVERAGE:0.5:5:2


# Pour chaque valeur, il calcule la moyenne avec les valeurs qu'il reçoit pour un laps de temps
# bps = (counter_now - counter_before) / (time_now - time_before) * 8
#set -x
cpt_out=0
tmp_date=$date_beg
idx=0
echo "temps initial : $date_beg/$(date -d @$date_beg +"%H:%M:%S") / valeur : $cpt_out"
rrdtool update counter.rrd $((date_beg)):${cpt_out}

# de 10:00 à 10:01
echo
echo "== update de 10:00 à 10:01 =="
cpt_old=$cpt_out
date_old=$tmp_date
((tmp_date+=60))
((cpt_out+=1024*4*60))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:01 à 10:02
echo
echo "== update de 10:01 à 10:02 =="
echo "Il est possible de cumuler plusieurs valeur pendant le heartbeat"
echo " Cela génère la moyenne pour les période <= heatbeat"
cpt_old=$cpt_out
date_old=$tmp_date
((tmp_date+=60*1-20))
((cpt_out+=1024*4*40))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
((tmp_date+=10))
((cpt_out+=1024*4*10))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
((tmp_date+=10))
((cpt_out+=1024*4*10))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:02 à 10:03
echo
echo "== update de 10:02 à 10:03 =="
echo "Cas compliqué"
echo "Notre dernière update est à 10:02:00"
echo "Nous indiquons que nous avons une update à 10:03:03"
echo "Hors le heatbeat est à 60 seconde donc max 10:03:00"
echo "La valeur est reconnu comme NA"
cpt_old=$cpt_out
date_old=$tmp_date
((tmp_date+=60+3))
((cpt_out+=1000000*1*63))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

rrdtool info  counter.rrd 
# de 10:04 à 10:05
echo 
echo "== update de 10:03 à 10:04 =="
echo "La dernière update est à 10:03:03"
echo "Ici on passe l'update à 10:04:02"
echo "comme le heatbeat est inférieur à 60 seconde, il l'accepte pour 10:04"
cpt_old=$cpt_out
date_old=$tmp_date
((tmp_date+=59))
((cpt_out+=1024*4*59))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:05 à 10:06
cpt_old=$cpt_out
date_old=$tmp_date
echo
echo "== update de 10:05 à 10:06 =="
echo "la dernière update est passé à 10:04:02 pour 10:04:00"
echo "De nouveau un problème, on passe l'update à 10:05:03 soit 61 secondes"
((tmp_date+=61))
((cpt_out+=1024*4*61))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:06 à 10:07
cpt_old=$cpt_out
date_old=$tmp_date
echo
echo "== update de 10:06 à 10:07 =="
((tmp_date+=57))
((cpt_out+=1024*4*57))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:07 à 10:08
cpt_old=$cpt_out
date_old=$tmp_date
echo
echo "== update de 10:07 à 10:08 =="
echo "Attention c'est la valeur maximum de la moyenne sur la duré!!!!"
echo "Donc comme c'est limité à 1000000, on a NA"
((tmp_date+=60))
((cpt_out+=(1000000*60)+1))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:08 à 10:09
cpt_old=$cpt_out
date_old=$tmp_date
echo
echo "== update de 10:08 à 10:09 =="
((tmp_date+=60))
((cpt_out+=1024*4*60))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:09 à 10:10
cpt_old=$cpt_out
date_old=$tmp_date
echo
echo "== update de 10:09 à 10:10 =="
((tmp_date+=60))
((cpt_out+=1024*4*60))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

# de 10:10 à 10:11
cpt_old=$cpt_out
date_old=$tmp_date
echo
echo "== update de 10:10 à 10:11 =="
((tmp_date+=60))
((cpt_out+=1024*4*60))
echo "=> valeur derniere update : $(date -d @$(rrdtool last counter.rrd ) +"%H:%M:%S")"
rrdtool update counter.rrd $tmp_date:$cpt_out
echo "valeur $((idx++)) : " \
	"$tmp_date / $(date -d @$tmp_date +"%H:%M:%S") / " \
	"valeur : $cpt_out / " \
	"heartbeat (60) : $((tmp_date-date_old)) / " \
	"valeur $(rrdtool lastupdate counter.rrd | awk -F':' '$0 ~ /^[0-9]+/ {print $2}')"
echo "=> moyenne : $(((cpt_out-cpt_old)/(tmp_date-date_old))) " \
	"info rrd moyenne : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].value/ {print $3}') " \
	"info rrd second : $(rrdtool info counter.rrd | awk '$1 ~ /ds\[ifOutOctets\].unknown_sec/ {print $3}') "

#rrdtool dump counter.rrd

# Create graph
rrdtool graph test_good.png -s ${date_beg} -e $((date_beg+60*10)) -h 300 -w 600 -t "Sorti" \
  DEF:Sorti=counter.rrd:ifOutOctets:AVERAGE LINE1:Sorti#FF0000:"routeur"
#eog test.png
